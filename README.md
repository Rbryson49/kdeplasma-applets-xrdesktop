xrdesktop applet for KDE Plasma
===============================

Plasmoid to toggle the xrdesktop KWin plugin.

The current status of xrdesktop is reflected by a check box.

## Requirements

* kwin-effect-xrdesktop

## Install

### ... with cmake

```
cmake -DCMAKE_INSTALL_PREFIX="`kf5-config --prefix`" ../kdeplasma-applets-xrdesktop
make install
```

### ... manually
```
$ git clone https://gitlab.freedesktop.org/xrdesktop/kdeplasma-applets-xrdesktop.git
$ kpackagetool5 -t Plasma/Applet -i kdeplasma-applets-xrdesktop/package
```

Now add the widget to your panel or the desktop. The widget is called "xrdesktop" in the widget list.

That's all!

Acknowledgements: based on https://github.com/rpallai/dap-locksw

## Code of Conduct

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms.

We follow the standard freedesktop.org code of conduct,
available at <https://www.freedesktop.org/wiki/CodeOfConduct/>,
which is based on the [Contributor Covenant](https://www.contributor-covenant.org).

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting:

* First-line project contacts:
  * Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
  * Christoph Haag <christoph.haag@collabora.com>
* freedesktop.org contacts: see most recent list at <https://www.freedesktop.org/wiki/CodeOfConduct/>

